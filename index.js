"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/** Assertion error class to identify assertion errors */
var AssertError = /** @class */ (function (_super) {
    __extends(AssertError, _super);
    function AssertError() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AssertError;
}(Error));
exports.AssertError = AssertError;
/**
 * Asserts given condition. If condition is invalid, error is trown.
 * @param condition valid value condition.
 * @param msg error message to throw, when condition is invalid.
 * @throws {AssertError} assertion error with assertion error message.
 */
function assert(condition, msg) {
    if (!condition) {
        throw new AssertError(msg);
    }
}
exports.assert = assert;
/**
 * Assertor class which can call multiple assertions and instead of firing exception
 * keeps error state in memory.
 */
var Assertor = /** @class */ (function () {
    function Assertor() {
        this._valid = true;
        this.assertCallback = this.assertCallback.bind(this);
        this.assertionBlock = this.assertionBlock.bind(this);
        this.throwInvalid = this.throwInvalid.bind(this);
    }
    Object.defineProperty(Assertor.prototype, "valid", {
        /**
         * Indicator showing if error has occured.
         */
        get: function () {
            return this._valid;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Asserts condition without throwing error.
     * @param condition - condition to check
     * @param callback - callback to call, if error occured.
     */
    Assertor.prototype.assertCallback = function (condition, callback) {
        try {
            assert(condition);
        }
        catch (error) {
            callback();
            this._valid = false;
        }
    };
    /**
     * Asserts array of conditions. Stops after first error encounter.
     * @param assertions [condition, callback] - condition and callback pair.
     */
    Assertor.prototype.assertionBlock = function () {
        var assertions = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            assertions[_i] = arguments[_i];
        }
        for (var _a = 0, assertions_1 = assertions; _a < assertions_1.length; _a++) {
            var _b = assertions_1[_a], condition = _b[0], callback = _b[1];
            try {
                assert(condition());
            }
            catch (error) {
                callback();
                this._valid = false;
                break;
            }
        }
    };
    /**
     * If exception during assertion was raised, then this function raises it.
     * @param error
     */
    Assertor.prototype.throwInvalid = function (error) {
        if (!this._valid) {
            throw new AssertError(error);
        }
    };
    return Assertor;
}());
exports.Assertor = Assertor;
